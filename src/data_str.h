#ifndef DATA_STR_H
#define DATA_STR_H

#include<string>

struct var_t {
    std::string chrom;
    long int position;
    std::string ref;
    std::string alt;

    // less comparator so var_t object can be put in a set
    bool operator<(const var_t& another) const {
        if(chrom != another.chrom) return chrom < another.chrom;
        if(position != another.position) return position < another.position;
        if(ref != another.ref) return ref < another.ref;
        return alt < another.alt;
    }

    // construct var_t object from string like "CHROM:POS:REF:ALT"
    static var_t fromString(const std::string& var_str) {
        var_t var;
        
        auto colon_pos = var_str.find(":");

        auto start = var_str.begin();
        auto end = var_str.begin() + colon_pos;
        
        var.chrom = std::string(start, end);
        start = end + 1;

        colon_pos = var_str.find(":", colon_pos + 1);
        end = var_str.begin() + colon_pos;
        var.position = std::stoi(std::string(start, end)) - 1;
        start = end + 1;

        colon_pos = var_str.find(":", colon_pos + 1);
        end = var_str.begin() + colon_pos;
        var.ref = std::string(start, end);
        start = end + 1;

        var.alt = std::string(start, var_str.end());

        return var;
    }
};
std::ostream& operator<<(std::ostream& os, const var_t& rhs) {
    os<<rhs.chrom<<":"<<rhs.position + 1 <<":"<<rhs.ref<<":"<<rhs.alt;
    return os;
}

struct genotype_t {
    int32_t DP;
    int32_t AO;
    int32_t RO;

    static genotype_t fromString(const std::string& gt_str) {
        genotype_t genotype;
        
        sscanf(gt_str.c_str(), "%d:%d:%d", &genotype.DP, &genotype.RO, &genotype.AO);

        return genotype;
    }
};
std::ostream& operator<<(std::ostream& os, const genotype_t& rhs) {
    os<<rhs.DP<<":"<<rhs.RO<<":"<<rhs.AO;
    return os;
}

struct assign1_t {
    std::map<std::string, double> posteriors;
    std::map<std::string, int> obs;
    std::map<std::string, int> positive_obs;
    std::map<std::string, int> def_obs;
    std::map<std::string, int> def_pos_obs;
};

typedef std::map<std::string, std::map<var_t, genotype_t>> genotypes_t;
#endif
