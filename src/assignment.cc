#include<fstream>
#include<iostream>
#include<map>
#include<set>
#include<vector>
#include<string>
#include<algorithm>
#include<numeric>
#include<cstdio>
#include<cmath>
#include<cassert>
#include<cstdlib>
#include<unistd.h>
#include<iterator>

#ifdef HAVE_CONFIG_H
#include<config.h>
#else
#define VERSION "0.0.0"
#endif

#include<yaml-cpp/yaml.h>

#include "../contrib/htslibpp/htslibpp.h"
#include "../contrib/htslibpp/htslibpp_variant.h"

#include "../contrib/log/log.h"

#include "data_str.h"

#ifndef LOGLVL
#define LOGLVL LOGLV_WARN
#endif

inline double logp2qual(double logP) {
    return -10.0 * log10(1-pow(10, logP));
}

auto logger = LOGGER(LOGLVL);

using namespace YiCppLib::HTSLibpp;

struct config_t {

    double likelihood[2][2];
    std::vector<std::string> clusters;
    std::map<std::string, std::set<var_t>> cluster_vars;

    std::vector<std::string> subclones;
    std::map<std::string, float> subclone_fractions;
    std::map<std::string, std::set<var_t>> subclone_vars;
    std::map<std::string, std::set<var_t>> subclone_defining_vars;
};

std::shared_ptr<config_t> load_config(const std::string& yaml_fn);
std::vector<var_t> load_variants(const std::string& vcf_fn);
genotypes_t load_genotypes(const std::string& gt_fn);
assign1_t assign1(const std::shared_ptr<config_t>& config, const genotypes_t& genotypes, const std::string& cell_barcode);

void usage(const char* progName);
void print_header(std::shared_ptr<config_t> config);
void print_record(const std::shared_ptr<config_t> config, const std::string& cell_barcode, const assign1_t& assignment);

int minQual = 3;

int main(int argc, char *const *argv){

    int opt;

    while((opt = getopt(argc, argv, "q:h")) != EOF)
        switch(opt) {
            case 'q': minQual = std::atoi(optarg); break;
            case 'h': usage(argv[0]); break;
            default:  std::cerr<<"unknown option "<<argv[optind]<<std::endl; usage(argv[0]); break;
        }

    if(argc < optind + 2) usage(argv[0]);

    auto config = load_config(argv[optind]);
    auto genotypes = load_genotypes(argv[optind + 1]);

    print_header(config);

    std::for_each(genotypes.cbegin(), genotypes.cend(), [&](const auto &genotype) {
        // perform assignment
        auto assignment = assign1(config, genotypes, genotype.first);
        
        logger(LOGLV_INFO)<<"assignment for "<<genotype.first<<" done"<<std::endl;

        print_record(config, genotype.first, assignment);
    });

    return 0;
}

void usage(const char* progName) {
    std::cout<<"scbayes assignment (version "<<VERSION<<")"<<std::endl;
    std::cout<<"Usage "<<progName<<" [options] <config-yaml> <genotype-csv>"<<std::endl;
    std::cout<<std::endl;
    std::cout<<"  OPTIONS:"<<std::endl;
    std::cout<<"    -q <min-qual>    [default=3] only assign cells with a posterior"<<std::endl;
    std::cout<<"                     probability Phred quality score greater than <min-qual>."<<std::endl;
    std::cout<<std::endl;
    std::cout<<"    -h               display this usage message"<<std::endl;
    exit(0);
}

void print_header(std::shared_ptr<config_t> config) {
    std::cout<<"Barcode\tASIG\tQUAL\tFORMAT\t";
    std::copy(config->subclones.cbegin(), config->subclones.cend() - 1, std::ostream_iterator<std::string>(std::cout, "\t"));
    std::cout<<*(config->subclones.cend() - 1)<<std::endl;
}

void print_record(const std::shared_ptr<config_t> config, const std::string& cell_barcode, const assign1_t& assignment) {
    // sort by posterior probability
    const auto posterior_sort = [](const std::pair<std::string, double>& elem1, const std::pair<std::string, double>& elem2){ return elem1.second > elem2.second; };
    std::multiset<std::pair<std::string, double>, decltype(posterior_sort)> sorted_posteriors(
            assignment.posteriors.begin(), 
            assignment.posteriors.end(), 
            posterior_sort);

    logger(LOGLV_INFO)<<"posterior sorted"<<std::endl;
    logger(LOGLV_INFO)<<"multiset_size: "<<sorted_posteriors.size()<<std::endl;

    // print barcode, assignment, quality, and format fields
    std::cout<<cell_barcode;
    if(sorted_posteriors.size() == 0) std::cout<<"\tUNASSIGN\t.";
    else if(logp2qual(sorted_posteriors.cbegin()->second) < minQual) std::cout<<"\tUNASSIGN\t.";
    else std::cout<<"\t"<<sorted_posteriors.cbegin()->first<<"\t"<<logp2qual(sorted_posteriors.cbegin()->second);

    std::cout<<"\tOBS:POBS:DOBS:DPOBS:QUAL";

    for(const auto& subclone : config->subclones) {
        if(assignment.posteriors.find(subclone) != assignment.posteriors.end()) {
            
            const auto& postpr = assignment.posteriors.at(subclone);
            const auto& obs = assignment.obs.at(subclone);
            const auto& pos_obs = assignment.positive_obs.at(subclone);
            const auto& def_obs = assignment.def_obs.at(subclone);
            const auto& def_pos_obs = assignment.def_pos_obs.at(subclone);
            std::cout<<"\t"<<obs<<":"<<pos_obs<<":"<<def_obs<<":"<<def_pos_obs<<":"<<logp2qual(postpr);
        }
        else std::cout<<"\t.:.:.:.:.";
    }

    std::cout<<std::endl;
}

std::shared_ptr<config_t> load_config(const std::string& yaml_fn) {

    auto cfg = std::make_shared<config_t>();

    auto config_yaml = YAML::LoadFile(yaml_fn);

    // check if custom data likelihoods are defined
    if(auto likelihood_node = config_yaml["data_likelihoods"]) {
        logger(LOGLV_INFO)<<"Loading custom data likelihoods"<<std::endl;
        cfg->likelihood[1][0] = likelihood_node["false_positive"].template as<double>(); // cell has alt reads when it shouldn't
        cfg->likelihood[1][1] = likelihood_node["true_positive"].template as<double>(); // cell has alt reads when it should
    } else {
        logger(LOGLV_INFO)<<"Using default data likelihoods"<<std::endl;
        cfg->likelihood[1][0] = 0.01; // cell has alt reads; subclone doesn't have variant
        cfg->likelihood[1][1] = 0.70; // cell has alt reads; subclone has variant (positive id)
    }

    cfg->likelihood[0][0] = 1.0 - cfg->likelihood[1][0];
    cfg->likelihood[0][1] = 1.0 - cfg->likelihood[1][1];
    
    logger(LOGLV_INFO)<<"Data likelihood matrix: "<<std::endl;
    logger(LOGLV_INFO)<<cfg->likelihood[0][0]<<"\t"<<cfg->likelihood[1][0]<<std::endl;
    logger(LOGLV_INFO)<<cfg->likelihood[0][1]<<"\t"<<cfg->likelihood[1][1]<<std::endl;

    cfg->likelihood[0][0] = log10(cfg->likelihood[0][0]);
    cfg->likelihood[0][1] = log10(cfg->likelihood[0][1]);
    cfg->likelihood[1][0] = log10(cfg->likelihood[1][0]);
    cfg->likelihood[1][1] = log10(cfg->likelihood[1][1]);

    // load cluster definitions
    std::for_each(config_yaml["clusters"].begin(), config_yaml["clusters"].end(), [&cfg](const auto node) {
        std::string cluster_name = node["name"].template as<std::string>();
        std::string cluster_vcf  = node["vcf"].template as<std::string>();

        logger(LOGLV_INFO)<<"Loading cluster "<<cluster_name<<" : "<<cluster_vcf<< " ... ";
        cfg->clusters.push_back(cluster_name);

        auto variants = load_variants(cluster_vcf);
        logger(LOGLV_INFO)<<variants.size()<<" variants loaded"<<std::endl;
        cfg->cluster_vars[cluster_name] = std::set<var_t>(variants.cbegin(), variants.cend());
    });

    // load subclone definitions
    std::for_each(config_yaml["subclones"].begin(), config_yaml["subclones"].end(), [&cfg](const auto node) {
        std::string subclone_name = node["name"].template as<std::string>();
        float subclone_fraction   = node["fraction"].template as<float>();
        std::string defining_cluster = node["defining_cluster"].template as<std::string>();
        std::set<var_t> subclone_vars;
        std::set<var_t> defining_vars;

        logger(LOGLV_INFO)<<"Loading subclone "<<subclone_name<<"["<<subclone_fraction<<"]  ... ";

        // insert variants of each cluster into subclone_vars
        std::for_each(node["clusters"].begin(), node["clusters"].end(), [&subclone_vars, &cfg](const auto cluster_node) {
            std::string cluster_name = cluster_node.template as<std::string>();
            subclone_vars.insert(cfg->cluster_vars[cluster_name].cbegin(), cfg->cluster_vars[cluster_name].cend());
        });

        logger(LOGLV_INFO)<<subclone_vars.size()<<" variants loaded"<<std::endl;

        defining_vars.insert(cfg->cluster_vars[defining_cluster].cbegin(), cfg->cluster_vars[defining_cluster].cend());

        cfg->subclones.push_back(subclone_name);
        cfg->subclone_fractions[subclone_name] = subclone_fraction;
        cfg->subclone_vars[subclone_name] = subclone_vars;
        cfg->subclone_defining_vars[subclone_name] = defining_vars;
    });

    return cfg;
}

std::vector<var_t> load_variants(const std::string& vcf_fn) {
    auto hfile = htsOpen(vcf_fn.c_str(), "r");
    auto header = htsHeader<bcfHeader>::read(hfile);

    if(header.get() == nullptr) {
        std::cerr<<"unable to parse vcf header"<<std::endl;
        exit(1);
    }

    // load reference id -> chromosome name lookup table from vcf header
    std::vector<std::string> id2ctg;
    id2ctg.reserve(header->n[BCF_DT_CTG]);

    std::transform(htsHeader<bcfHeader>::dictBegin(header, htsHeader<bcfHeader>::DictType::CONTIG), 
            htsHeader<bcfHeader>::dictEnd(header, htsHeader<bcfHeader>::DictType::CONTIG),
            std::back_inserter(id2ctg),
            [](const auto &rec) -> std::string { HTSProxyIDPairContig proxy(rec); return proxy.key(); });

    // load variants from vcf
    std::vector<var_t> variants;
    variants.reserve(2000);

    std::transform(begin(hfile, header), end(hfile, header), std::back_inserter(variants), [&id2ctg](const auto &rec) -> var_t {
        bcf_unpack(rec.get(), BCF_UN_STR);
        return {id2ctg[rec->rid], rec->pos, rec->d.allele[0], rec->d.allele[1]};
    });

    variants.shrink_to_fit();

    return variants;
}

genotypes_t load_genotypes(const std::string& gt_fn) {

    genotypes_t genotypes;

    std::string line;
    std::map<size_t, std::string> barcode_lookup;

    std::ifstream infile(gt_fn);
    
    if(!std::getline(infile, line)) return genotypes;

    std::stringstream header_stream(line);
    std::string token;

    int col = 0;

    // iterate over header columns
    logger(LOGLV_INFO)<<"loading header line of genotype file ... ";
    while(std::getline(header_stream, token, '\t')) {
        if(col == 0) {
            col++;
            continue;
        }

        barcode_lookup[col] = token;
        col++;
    }

    logger(LOGLV_INFO)<<barcode_lookup.size()<<" barcodes loaded"<<std::endl;

    logger(LOGLV_INFO)<<"loading genotype lines ... ";

    // iterate over lines
    while(std::getline(infile, line)) {
        col = 0;
        std::stringstream var_stream(line);
        var_t variant;

        // iterate over columns
        while(std::getline(var_stream, token, '\t')) {
            if(col == 0) {
                variant = var_t::fromString(token);
                col++;
                continue;
            }

            auto genotype = genotype_t::fromString(token);
            const auto &cb = barcode_lookup[col];

            if(genotypes.find(cb) == genotypes.end()) {
                std::map<var_t, genotype_t> cell_gt_map;
                genotypes[cb] = cell_gt_map;
            }

            genotypes[cb][variant] = genotype;

            col++;
        }
    }

    logger(LOGLV_INFO)<<genotypes.size()<<" cells genotype loaded"<<std::endl;

    logger(LOGLV_INFO)<<genotypes[barcode_lookup[1]].size()<<" genotypes per cell loaded"<<std::endl;

    return genotypes;
}

assign1_t assign1(const std::shared_ptr<config_t>& config, const genotypes_t& genotypes, const std::string& cell_barcode) {
    assign1_t result;
    
    std::for_each(config->subclones.cbegin(), config->subclones.cend(), [&](const auto& subclone) {
        const auto& cell_genotypes = genotypes.find(cell_barcode)->second;
        double cum_likelihood = 0;
        int obs = 0;
        int positive_obs = 0;
        int def_obs = 0;
        int def_pos_obs = 0;
        bool is_subclone_normal = config->subclone_vars[subclone].size() == 0 ? true : false;

        for(const auto& var : cell_genotypes) {

            if(var.second.DP == 0) continue;
            
            auto fit = config->subclone_vars[subclone].find(var.first);
            int idx1 = var.second.AO > 0 ? 1 : 0;
            int idx2 = fit != config->subclone_vars[subclone].end() ? 1 : 0;

            obs++;
            if(idx1 == 1 && idx2 == 1) positive_obs++;
            
            if(config->subclone_defining_vars[subclone].find(var.first) != config->subclone_defining_vars[subclone].end()) {
                def_obs++;
                if(idx1 == 1 && idx2 == 1) def_pos_obs++;
            }

            cum_likelihood += config->likelihood[idx1][idx2];
        }

        // sanity checks
        assert(positive_obs <= obs);
        assert(def_obs <= obs);
        assert(def_pos_obs <= def_obs);

        result.posteriors[subclone] = cum_likelihood + log10(config->subclone_fractions[subclone]);
        result.obs[subclone] = obs;
        result.positive_obs[subclone] = positive_obs;
        result.def_obs[subclone] = def_obs;
        result.def_pos_obs[subclone] = def_pos_obs;
    });

    // readjust log likelihoods
    auto adjust_it = std::max_element(result.posteriors.cbegin(), result.posteriors.cend(), [](auto& p1, auto& p2) { return p1.second < p2.second; });
    auto adjust_factor = adjust_it->second;
    std::for_each(result.posteriors.begin(), result.posteriors.end(), [&adjust_factor](auto& pair) { pair.second -= adjust_factor; });

    auto normalization = std::accumulate(result.posteriors.cbegin(), result.posteriors.cend(), 0.0, 
            [](const auto& a, const auto&b) -> double {return a + pow(10, b.second);});
    logger(LOGLV_INFO)<<"normalization factor = "<<normalization<<std::endl;
    std::for_each(result.posteriors.begin(), result.posteriors.end(), [&normalization](auto& pair) { pair.second -= log10(normalization); });

    return result;
}
