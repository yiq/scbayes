#ifdef HAVE_CONFIG_H
#include<config.h>
#endif
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
#include<iterator>
#include<iostream>
#include<algorithm>
#include<string.h>
#include<map>

#include<unistd.h>

#include "../contrib/hileup/hile.h"

#include "../contrib/htslibpp/htslibpp.h"
#include "../contrib/htslibpp/htslibpp_variant.h"
#include "../contrib/log/log.h"

#include "data_str.h"

#ifndef LOGLVL
#define LOGLVL LOGLV_ERR
#endif

auto logger = LOGGER(LOGLVL);

using namespace YiCppLib::HTSLibpp;

// private function declaretions
void usage(const char* progName);
std::vector<std::string> load_barcodes(const std::string& tsv_fn);
std::vector<var_t> load_variants(const std::string& vcf_fn);
std::vector<std::vector<genotype_t>> genotype(const std::string& bam_fn, const std::vector<var_t>& variants, const std::vector<std::string>& barcodes, const bool useRG = false);
void print_output(const std::vector<std::string>& barcodes, const std::vector<var_t>& variants, const std::vector<std::vector<genotype_t>>& genotypes);

int main(int argc, char** argv) {

    bool useRG = false;
    int opt;
    while((opt = getopt(argc, argv, "r")) != -1) {
        switch(opt) {
            case 'r':
                useRG = true;
                break;
            default:
                break;
        }
    }

    
    logger(LOGLV_DEBUG)<<"optind: "<<optind<<std::endl;
    
    // expecting three more args
    if(optind + 2 >= argc) usage(argv[0]);

    logger(LOGLV_INFO)<<"barcode file: "<<argv[optind + 1]<<std::endl;
    logger(LOGLV_INFO)<<"vcf file: "<<argv[optind + 2]<<std::endl;
    logger(LOGLV_INFO)<<"bam file: "<<argv[optind]<<std::endl;

    try {
        auto barcodes  = load_barcodes(argv[optind + 1]);
        auto variants  = load_variants(argv[optind + 2]);
        auto genotypes = genotype(argv[optind], variants, barcodes, useRG);
        print_output(barcodes, variants, genotypes);
    } catch(int err) {
        return err;
    }

    return 0;
}

// private function definitions
void usage(const char* progName) {
    std::cout<<"scbayes genotype (version "<<VERSION<<")"<<std::endl;
    std::cout<<"Usage: " << progName << " [-r] <cellranger-bam> <barcode-txt> <variants-vcf>" << std::endl;
    std::cout<<std::endl;
    std::cout<<"       -r        Use read group ID instead of CB tags for cell barcodes."<<std::endl;
    std::cout<<"                 This is useful when dealing with fluidigm results where"<<std::endl;
    std::cout<<"                 each cell will have its own bam file."<<std::endl;
    exit(0);
}

std::vector<std::string> load_barcodes(const std::string& tsv_fn) {
    std::ifstream tsv_in(tsv_fn);

    if(!tsv_in.is_open()) {
        logger(LOGLV_ERR)<<"Unable to open barcode file: "<<tsv_fn<<std::endl;
        throw 1;
    }

    auto fbegin = std::istream_iterator<std::string>(tsv_in);
    auto fend = std::istream_iterator<std::string>();

    std::vector<std::string> barcodes(fbegin, fend);
    return barcodes;
}

std::vector<var_t> load_variants(const std::string& vcf_fn) {
    auto hfile = htsOpen(vcf_fn.c_str(), "r");

    if(hfile == NULL) {
        logger(LOGLV_ERR)<<"Unable to open vcf file: "<<vcf_fn<<std::endl;
        throw 2;
    }

    auto header = htsHeader<bcfHeader>::read(hfile);

    // load reference id -> chromosome name lookup table from vcf header
    std::vector<std::string> id2ctg;
    id2ctg.reserve(header->n[BCF_DT_CTG]);

    std::transform(
            htsHeader<bcfHeader>::dictBegin(header, htsHeader<bcfHeader>::DictType::CONTIG), 
            htsHeader<bcfHeader>::dictEnd(header, htsHeader<bcfHeader>::DictType::CONTIG),
            std::back_inserter(id2ctg),
            [](const auto &rec) -> std::string { HTSProxyIDPairContig proxy(rec); return proxy.key(); });

    // load variants from vcf
    std::vector<var_t> variants;
    variants.reserve(2000);
    std::transform(begin(hfile, header), end(hfile, header), std::back_inserter(variants), [&id2ctg](const auto &rec) -> var_t {
        bcf_unpack(rec.get(), BCF_UN_STR);
        return {id2ctg[rec->rid], rec->pos, rec->d.allele[0], rec->d.allele[1]};
    });
    variants.shrink_to_fit();

    return variants;
}

std::vector<std::vector<genotype_t>> genotype(
        const std::string& bam_fn, 
        const std::vector<var_t>& variants, 
        const std::vector<std::string>& barcodes,
        const bool useRG) {

    // cell-barcode to index lookup table
    std::map<std::string, size_t> cb_lookup;
    for(int i=0; i<barcodes.size(); i++) cb_lookup[barcodes[i]] = i;

    const genotype_t empty_genotype{0, 0, 0};
    std::vector<std::vector<genotype_t>> genotype_matrix;

    ::htsFile *htf = hts_open(bam_fn.c_str(), "rb");

    if(htf == NULL) {
        logger(LOGLV_ERR)<<"Unable to open bam file: "<<bam_fn<<std::endl;
        throw 3;
    }

    bam_hdr_t *hdr = sam_hdr_read(htf);
    hts_idx_t *idx = sam_index_load(htf, bam_fn.c_str());

    if(idx == NULL) {
        logger(LOGLV_ERR)<<"Unable to open bam index for bam file: "<<bam_fn<<std::endl;
        throw 4;
    }

    std::for_each(variants.cbegin(), variants.cend(), [&](const auto &var) {

        logger(LOGLV_INFO)<<"processing variant "<<var<<std::endl;

        // initialize a row in genotype matrix with empty genotypes
        std::vector<genotype_t> gt_row(barcodes.size(), empty_genotype);

        // initialize hileup engine
        hile_config_t cfg = hile_init_config();
        cfg.track_base_qualities = false;
        cfg.track_mapping_qualities = true;
        cfg.track_read_names = true;

        if(useRG) {
            cfg.tags[0] = 'R'; cfg.tags[1] = 'G';
        }
        else {
            cfg.tags[0] = 'C'; cfg.tags[1] = 'B';
        }

        hile* h = hileup(htf, hdr, idx, var.chrom.c_str(), var.position, &cfg);
        for(int i=0; i<h->n; i++) {

            char pileup_base = (char)h->bases[i].base;
            char ref_base = var.ref[0];
            char alt_base = var.alt[0];

            // check if tagged cell barcode is a cell. Skip read if not.
            auto search_cb = cb_lookup.find(h->tags[i]);
            if(search_cb == cb_lookup.end()) {
                logger(LOGLV_INFO)<<"cell barcode ["<<h->tags[i]<<"] does not exist in barcode list, skipping..."<<std::endl;
                continue;
            }

            logger(LOGLV_INFO)<<h->bases[i].base<<"\t"<<h->tags[i];

            size_t cell_idx = search_cb->second;
            if(pileup_base == ref_base) {
                gt_row[cell_idx].DP++;
                gt_row[cell_idx].RO++;
                logger(LOGLV_INFO)<<"\tREF++";
            }
            else if(pileup_base == alt_base ) {
                gt_row[cell_idx].DP++;
                gt_row[cell_idx].AO++;
                logger(LOGLV_INFO)<<"\tALT++";
            }
            //else {
            //    gt_row[cell_idx].DP++;
            //    logger(LOGLV_INFO)<<"\tDEP++";
            //}
            logger(LOGLV_INFO)<<std::endl;
        }

        genotype_matrix.push_back(gt_row);
    });

    return genotype_matrix;
}

void print_output(const std::vector<std::string>& barcodes, const std::vector<var_t>& variants, const std::vector<std::vector<genotype_t>>& genotypes) {
    std::cout<<"variant\t";
    std::copy(barcodes.cbegin(), barcodes.cend()-1, std::ostream_iterator<std::string>(std::cout, "\t"));
    std::cout<<*(barcodes.cend()-1)<<std::endl;

    for(int i=0; i<variants.size(); i++) {
        std::cout<<variants[i]<<"\t";
        std::copy(genotypes[i].cbegin(), genotypes[i].cend()-1, std::ostream_iterator<genotype_t>(std::cout, "\t"));
        std::cout<<*(genotypes[i].cend()-1)<<std::endl;
    }
}
