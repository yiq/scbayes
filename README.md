# Introduction

scBayes assigns single cells from scRNA and scATAC sequencing to subclones
reconstructed from bulk DNA sequencing. It utilizes a Bayesian probabilistic
framework to deal with the sparseness of the sequencing coverage

# Docker Image

scBayes can be run directly as a docker image. This is especially handy if you
work in a HPC environment where singularity is available. To run, use the
following commands as a reference:
```shell
$ singularity run docker://qiaoy/scbayes scGenotype
scbayes genotype (version 0.1.1)
Usage: scGenotype <cellranger-bam> <barcode-txt> <variants-vcf>

$ singularity run docker://qiaoy/scbayes scAssign
scbayes assignment (version 0.1.1)
Usage scAssign [options] <config-yaml> <genotype-csv>

  OPTIONS:
    -q <min-qual>    [default=3] only assign cells with a posterior
                     probability Phred quality score greater than <min-qual>.

    -h               display this usage message
```

# Installation

scBayes is distributed as source code. The building system is based on GNU
autotools. It depends on a c++ compiler that supports c++14; and several
software packages

## Dependencies

### c++14

You will need at least GCC 4.9 to build scBayes. The configure script will
automatically check if your compiler supports c++14; and would fail if it does
not.

### htslib >= 1.9

use `--with-htslib=` option with the `configure` script to specify where htslib
header files and libraries are located. If the specified path is
`$HTSLIB_ROOT`, then the configure script will be expecting to find htslib
header files at `$HTSLIB_ROOT/include/htslib`, and libraries at
`$HTSLIB_ROOT/lib/`. In addition, because htslib library is dynamically linked,
the path `$HTSLIB_ROOT/lib` also needs to be in your `$LD_LIBRARY_PATH` (linux)
or `$DYLD_LIBRARY_PATH` (mac) if it is not one of the standard locations. As an
example, if you installed htslib in the following ways:

```shell
curl -#LO https://github.com/samtools/htslib/releases/download/1.9/htslib-1.9.tar.bz2
tar -xf htslib-1.9.tar.bz2
cd htslib-1.9
./configure --prefix=/usr/local
make
make install
```

Then, you will want to use `--with-htslib=/usr/local` with the configure script.

### yaml-cpp >= 0.6.2

use `--with-yaml-cpp=` option with the configure script to specify where
yaml-cpp header files and libraries are located. If the specified path is
`$YAML_CPP_ROOT`, then the configure script will be expecting to find yaml-cpp
header files at `$YAML_CPP_ROOT/include/yaml-cpp`, and libraries at
`$YAML_CPP_ROOT/lib`. Since yaml-cpp library is statically linked, there is no
need to fiddle with `$LD_LIBRARY_PATH` environment variable. As an example, if
you installed yaml-cpp in the following ways:

```shell
curl -#LO https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-0.6.2.tar.gz
tar -xf yaml-cpp-0.6.2.tar.gz
cd yaml-cpp-yaml-cpp-0.6.2
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..  # yaml-cpp require cmake to build
make
make install
```

Then, you will want to use `--with-yaml-cpp=/usr/local` with the configure
script.

### Building and installing scBayes

It is recommended that you build scBayes in a separate directory from the
source tree. Use the following as a reference for your own build

```shell
git clone --recursive https://gitlab.com/yiq/scbayes  # --recursive is important
cd scbayes
git checkout 1.0.0 # if you want to build a specific version
mkdir build
cd build
../configure --with-htslib=/opt/htslib/1.9 --with-yaml-cpp=/opt/yaml-cpp/0.6.3 # I also made sure that /opt/htslib/1.9/lib is in my $LD_LIBRARY_PATH environment variable
make
sudo make install # this will install scbayes binaries into /usr/local/bin, therefore needs sudo; if you specify a different installation prefix using --prefix, you may not need sudo.
```

You should now see two files installed at the prefix path (or `/usr/local/bin`
by default), namely `scGenotype` and `scAssign`. We will discuss their
usage below.

# Using scBayes

scBayes runs in two stages. Stage 1 is to genotype the single cells at the
somatic variant locations to determine the amount of sequencing coverage, and
the number of reads containing either the reference or the alternate allele. We
use the program `scGenotype` for this purpose. Stage 2 is to assign single
cells to subclones based on the subclone structure, the variants in each
subclone, and the single cell genotypes. We use the program `scAssign` for
this purpose.

## scGenotype

We can get the usage for `scGenotype` by running the program without any
parameters

```shell
$ scGenotype
Usage: scGenotype <cellranger-bam> <barcode-txt> <variants-vcf>
```

* `cellranger-bam`: The bam file produced by the cellranger pipeline.
  `scGenotype` is specifically designed to process the bam file from the
  cellranger pipeline. But if your sequencing is from a different technology,
  `scGenotype` can still be run as long as reads are tagged with the `CB` tag
  for their originating cell barcode.
* `barcode-txt`: a text file listing cell barcodes that are believed to be
  cells. It can be just the cellranger produced cell barcodes (located at
  `$CELLRANGER_RESULT_DIR/outs/filtered_feature_bc_matrix/barcodes.tsv.gz`,
  decompressed), or if you use a different or additional filtering (e.g.
  Seurat), a custom created file conforming to the same format (i.e. a valid
  cell barcode per line)
* `variants-vcf`: a valid VCF file that contains **all** somatic variants. This
  can be considered as the union of all subclone defining variants.

Running `scGenotype` will produce output to the stdout, which can be redirected
to a file using the `>` redirect operator. It is a tab-delimited format with
each row corresponding to a variant in the vcf file, and each column
corresponding to a cell in the cell-barcode file. The table cells record how
many sequencing reads (DP) are found, how many of them contains the reference
allele (RO), and how many of them contains the variant allele (AO), at a
variant location (specified by row) and in a given cell (specified by column).
This file will be used as one of the input files to the next stage.

## scAssign

We can get the usage of `scAssign` by running the program without any arguments

```shell
$ scAssign
Usage scAssign <config-yaml> <genotype-csv>
```

* `config-yaml`: a yaml file that describes a particular assignment run. An
  example file looks like this:

  ```yaml
  clusters:
      - name: C1
        vcf: Cluster1.vcf
      - name: C2
        vcf: Cluster2.vcf
  subclones:
      - name: normal
        fraction: 0.1
        clusters: []
        defining_cluster: ""
      - name: subclone1
        fraction: 0.7
        clusters: [C1]
        defining_cluster: C1
      - name: subclone2
        fraction: 0.2
        clusters: [C1, C2]
        defining_cluster: C2
  ```

  The yaml file contains two main sections. The first section `clusters`
  contains the definitions of each subclone defining cluster. Each cluster is
  defined by a `name`, and a `vcf` file containing the variants in that
  cluster. **Note**: this vcf file should be a subset of the `variants-vcf`
  file given to `scGenotype` in stage 1; and that the vcf files in the
  `clusters` section of the yaml file should be mutually exclusive. The second
  section `subclones` describes the subclone structure from bulk DNA
  sequencing. Each subclone is defined by the following properties	

  * `name`: a name associated to the subclone
  * `fraction`: the population fraction of this subclone. This will be used as
    the prior for Bayesian posterior probability calculations
  * `clusters`: a list of cluster names defined in the `clusters` section. In
    this example yaml, `subclone1` is the founding clone, and has the variants
    in `C1`; `subclone2` is a descendent from `subclone1`, inheriting the `C1`
    variants, but also has extra mutations `C2`
  * defining_cluster: the name of the cluster with which this subclone is
    defined. It is the new variants not found in its parent clone. In this
    example `C1` is the defining cluster of `subclone1`; and `C2` is the
    defining cluster of `subclone2`.

* `genotype-csv`: the genotype file produced by `scGenotype`. 

The output is a tab-delimited text file that has the following columns:

```
Barcode, Assignment, AssignmentQuality, FORMAT, Subclone1AssignmentDetails, Subclone2AssignmentDetails, ...
```

* ` Barcode`: the cell barcode this record is about
* `Assignment`: the assignment result, which should be the subclone with the
  highest posterior probability. Two caveats: 1) the highest posterior
  probability may not be very high compared to the others. This would be
  reflected in the next colomn `AssignmentQuality`, which can be used to
  further filter out cells. And 2) if the cell does not have sequencing
  coverage at any somatic variant sites, the assignment result will be
  `UNASSIGN` to indicate that no data is present to allow assignment. 
* AssignmentQuality: A [Phred quality
  score](https://en.wikipedia.org/wiki/Phred_quality_score) representing the
  likelihood that the assignment could be wrong. The larger the number, the
  less likely the assignment is wrong; or in other words, the assignment result
  is more certain. This can be useful when it comes to filter out cells with
  very sporadic coverage.
* `FORMAT`: describes how the data in the rest of the columns are organized.
  The rest of the columns describe the posterior probability of this cell for
  each subclone described in the `config-yaml`, including the normal. Currently
  this column is hard-coded to be `OBS:POBS:DOBS:DPOBS:QUAL`:
  * `OBS`: The number of somatic variant sites (of **all** subclones) that have
    sequencing coverage in this cell
  * `POBS`: Positive observations. The number of somatic variant sites (that
    **this** subclone contains) that this cell showed variant allele reads
  * `DOBS`: Defining observations. The number of somatic variant sites (that
    defines this particular subclone) that have sequencing coverage in this
    cell. Note that the following should always be true: `DOBS <= OBS`
  * `DPOBS`: Defining positive observations. The number of somatic variant
    sites (that defines this particular subclone) that this cell showed variant
    allele reads.
  * `QUAL`: the Phred quality score of assigning this cell to this subclone.
* `Subclone1AssignmentDetails, ...`: For each subclone, a hypothesis is tested
  to see whether the cell had come from it. The details are reported in the
  format described in the `FORMAT` column. For example, a potential output can
  be `6:3:4:3:50`, which means that this cell has coverage at 6 somatic variant
  sites, out of which 3 sites have reads that contain the variant allele. This
  cell has coverage at 4 somatic variant sites which defines this subclone
  specifically, and out of which 3 positions have reads that contain the
  variant allele. The assignment quality for this cell to this subclone is 50,
  which means the posterior probability is 99.999%. A special value is
  `.:.:.:.:.`, which means that, due to the fact no somatic variant sites are
  covered by sequencing, assignment was not attempted.

Just as an example, here is a sample output with two cells

```
AAACCTGGTCTAGTGT-1 UNASSIGN  .       OBS:POBS:DOBS:DPOBS:QUAL .:.:.:.:.     .:.:.:.:.     .:.:.:.:.
AAACCTGGTCTGCCAG-1 subclone2 52.2774 OBS:POBS:DOBS:DPOBS:QUAL 3:0:0:0:1e-05 3:0:0:0:1e-05 3:3:3:3:52.2774
```

The first cell is not assigned because it lacks coverage at somatic variant
sites. The second cell is assigned to `subclone2` with a Phred quality score of
52.2774 (which means scBayes is roughly 99.999% confident about the assignment
result). Beyond the `FORMAT` field, there are three more columns, which
corresponds to `normal`, `subclone1`, and `subclone2`. Let's take cell 2 for an
example. This cell has sequencing coverage at 3 somatic variant sites (`OBS`).
This cell has variant alleles at 0 sites that the normal subclone contains
(`POBS`), which is expected since the normal subclone contains no somatic
mutations. It has variant alleles at 0 sites that subclone1 contains, and 3
sites that subclone2 contains. Furthermore, for subclone2, this cell has
sequencing coverage at 3 defining somatic variant sites (`DOBS`), out of which
3 showed variant allele (`PDOBS`). Taken together, the likelihood, on a Phred
score scale, is 1E-5 for this cell to belong to normal; 1E-5 to subclone1; and
52.2774 for subclone2. 

This file is designed to be versatile. If you take the first two columns, you
get a valid feature file that can be directly loaded into [Loupe
Browser](https://support.10xgenomics.com/single-cell-gene-expression/software/visualization/latest/what-is-loupe-cell-browser)
(10X Genomic) for visualization. But if you want to investigate the assignment
details, and potentially implement custom filtering logic, you also have the
full width of details to work with.

